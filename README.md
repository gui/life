# Instructions de compilation

## Compilation sur Linux

### La compilation sur Linux est on ne peut plus simple. 

1 – Exécutez les commandes suivantes:

`$ sudo apt update$ sudo apt install libsdl1.2-dev -y`

2 – Rendez-vous dans le dossier du code avec:

`$ cd chemin/vers/le/dossier`

3 – Utilisez GCC pour compiler:

`$ gcc -o life main.c 'sdl-config --cflags –libs'`

4 – Lancez le programme:

`$ ./life`



## Compilation sur Windows

1 – Installez MinGW,compilateur GCC pour Windows, en téléchargement [ici](https://osdn.net/projects/mingw/releases/)

2 – Configurez les PATHS

- Ouvrez « Modifier les variables d’environnement système » en recherchant «path» dans le menu démarrer.
- Cliquez sur variables d’environnement.
- Double-cliquez sur «Path» sous « Variables système » puis sur « Nouvelle ».
- Collez « C:\MinGW\bin » et valider en cliquant sur OK aux trois fenêtres.

3 – Installez la SDL

- Téléchargez la SDL [ici](https://www.libsdl.org/download-1.2.php), version pour Mingw32.
- Extrayez le dossier SDL-x.x.x avec un logiciel comme 7-zip
- Copiez le contenu du sous-dossier \lib dans C:\MinGW\lib
- Copiez le contenu du sous-dossier \include dans C:\MinGW\include
- Copiez sdl-config de \bin vers C:\MinGW\msys\1.0\bin
- Copiez, s’il n’y est pas déjà, le fichier SDL.dll de \bin vers le répertoire du code.

4 – Compilez

- Dans un terminal (cmd.exe), exécutez: `$ gcc -o life main.c -lmingw32 -lSDLmain -lSDL5`
- Lancez le programme à l’aide d’un simple double-clic.