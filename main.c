/*	LIFE - 'main.c' - Abou CISSE, Arsen GOZOGLU, Guilhem TISSOT
	Version: α2 (03/12/2018)
	Fichier de lancement de LIFE - le jeu de la vie. */


#include <stdio.h>
#include <stdlib.h>
#include "jeuvie.c"


int main(int argc, char ** argv){
	/*	main() - fonction du projet LIFE
		Fonction principale du programme.
		Prend en arguments:
			- argc: le nombre d'arguments spécifiés dans la CLI au lancement
			- argv: les arguments sont stockés dans un tableau de caractères
		Retourne:
			- 0 si les arguments spécifiés sont invalides. */


    /* Vérification de la validité des arguments spécifiés en CLI conformément à:
		argument 1 : abcisse max souhaité
		argument 2 : ordonné max souhaité */

	int i,j;
	if(argc == 3){ // S'il y a bien 3 arguments spécifiés (l'ouverture du fichier en CLI compte comme un argument)
		for(j=1 ; j<3 ; ++j){
			for( i=0 ; i<strlen(argv[j]) ; ++i){
				if(!isdigit(argv[j][i])){ // Si les paramètres stockés dans le tableau de caractères ne sont pas des nombres
					printf("Les arguments spécifiés ne sont pas corrects. Indiquez la taille souhaitée pour le tableau en chiffres.\n");
					return 0;
				}
			}
		}
	}

	// Bonjour
	printf("      #\n      # ###### #    #       #####  ######       #        ##         #    # # ###### \n      # #      #    #       #    # #            #       #  #        #    # # #      \n      # #####  #    #       #    # #####        #      #    #       #    # # #####  \n#     # #      #    #       #    # #            #      ######       #    # # #      \n#     # #      #    #       #    # #            #      #    #        #  #  # #      \n #####  ######  ####        #####  ######       ###### #    #         ##   # ###### \n\n");

	int* jeu; // Pointeur pointant vers un entier.
	int fin = 0; // Le jeu se termine si fin vaut 1.
	int x = (argc == 3) ? atoi(argv[1]) : 15; // Si l'utilisateur a spécifié la dimension du tableau, on alloue
	int y = (argc == 3) ? atoi(argv[2]) : 10; // à x et y les valeurs spécifiées, sinon 15 et 10 respectivement.
	jeu = malloc(sizeof(int) * (x*y)); // On alloue à la grille la taille d'autant d'entiers que l'utilisateur voudra.
	char commande[70];

	initialiser_jeu(jeu, x*y); // x*y taille du tableau
	afficher_jeu(jeu,x,y);

	while(!fin){
		printf("> "); // interface d'écriture avec >
		lire(commande,30); // scanf pour éviter le buffer overflow, stocké dans commande char de 70 char
		formatage_commande(commande); // tout en minuscule pas nécessaire
		interpreteur_commande(commande,jeu,&x,&y,&fin);
		if(!fin) // pas besoin d'accolades si une seule instruction après le if
		afficher_jeu(jeu,x,y);
	}

}
