/*	LIFE - 'jeuvie.c' - Abou CISSE, Arsen GOZOGLU, Guilhem TISSOT
	Version: α2 (décembre 2018)
	Fichier contenant les fonctions principales de LIFE - le jeu de la vie. */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


void initialiser_jeu(int *jeu, int taille){
	/*	initialiser_jeu() - fonction du projet LIFE
		Fonction d'initialisation de la grille.
		Il initialiser toutes les cases de la grille à 0.
		Prend en arguments:
			- *jeu : un pointeur vers le tableau d'entiers 'jeu'
			- taille : 'x*y', la taille de la grille spécifiée par l'utilisateur ou bien prédéfinie */
	int i; 
	for(i=0; i<taille; ++i){
		*(jeu+i) = 0;
	}
}


int place_dans_grille(int x, int y, int tx, int ty){
	/*	place_dans_grille() - fonction du projet LIFE
		Convertie des coordonnées (x,y) vers un nombre unique (la place de la case dans le tableau).
		Prend en arguments:
			- x et y, les coordonnées à convertir
			- tx et ty: les dimensions de la grille
		Retourne:
			- -1 en cas d'erreur
			-  */

	int cx = 0, cy = 0, n = tx - 1, c = 0;

	for(cx = 0; cx <= tx; ++cx){
		if(cx > tx || cy > ty) return -1;
		if(!y){
			if(cx == x && cy == y) return c;
		}
		if(c == n){
			n += tx;
			cx = 0;
			cy++;
		}
		c++;
		if(cx == x && cy == y) return c;
	}
}


int place_vers_coordonnes(int place, int*x, int *y, int tx, int ty){
	int x1, y1;
		for(x1=0;x1<tx;++x1)
		{
		    for(y1=0;y1<ty; ++y1){
		        if(place_dans_grille(x1,y1,tx,ty) == place){
		            *x = x1; *y = y1;
		            return 1;
		        }
		    }
		}
		return 0;
}

void viderBuffer(){
	// Fonction emrunté à internet
    int c = 0;
    while (c != '\n' && c != EOF) c = getchar();
}
 
int lire(char *chaine, int longueur){
	// Fonction emrunté à internet
    char *positionEntree = NULL;
 
    if (fgets(chaine, longueur, stdin) != NULL){
        positionEntree = strchr(chaine, '\n');
        if (positionEntree != NULL){
            *positionEntree = '\0';
        }
        else{
            viderBuffer();
        }
        return 1;
    }
    else{
        viderBuffer();
        return 0;
    }
}



int valeur_tableau(int*tab, int val, int taille){
	// Retourne 1 si val est présent dans le tabelau.
	int i;
	for(i=0;i<taille;++i){
		if(tab[i] == val) return 1;
	}
	return 0; 
}


void afficher_jeu(int *jeu, int x, int y){

	int i,j, n = x-1, started = valeur_tableau(jeu,1,x*y);

	printf("\n\n");

	for(i=0; i<x*y; ++i){
		printf("%d ", (started) ? jeu[i]:i); // Si le tableau est vide, la fonction affiche le numéro des cases
		if(i == n){								// Il affiche les valeurs du tableau sinon
			printf("\n");
			n += x;
		}
	}
	printf("\n\n");
}



int nombre_voisin(int *tab, int x, int y, int tx, int ty){
	// retourne le nombres de voisins d'une case
	/* x,y : case souhaitée
	tx,ty : dimension du tableau
	*/
	int nombre_voisin = 0;

	if((x-1) >= 0 && (y-1)>=0){
		if(tab[place_dans_grille(x-1 , y-1 ,tx,ty)] == 1) nombre_voisin++;
	}
	if(y-1 >= 0){
		if(tab[place_dans_grille(x , y-1 ,tx,ty)] == 1) nombre_voisin++;
	}
	if((x+1) <= tx && (y-1) >= 0){
		if(tab[place_dans_grille(x+1 , y-1 ,tx,ty)] == 1) nombre_voisin++;
	}
	if((x-1) >= 0){
		if(tab[place_dans_grille(x-1 , y ,tx,ty)] == 1) nombre_voisin++;
	}
	if((x+1) <= tx){
		if(tab[place_dans_grille(x+1 , y ,tx,ty)] == 1) nombre_voisin++;
	}

	if((y+1) <= ty){
		if(tab[place_dans_grille(x , y+1 ,tx,ty)] == 1) nombre_voisin++;
	}
	if( (x-1) >= 0 && (y+1) <= ty){
		if(tab[place_dans_grille(x-1 , y+1 ,tx,ty)] == 1) nombre_voisin++;
	}
	if( (x+1) <= tx && (y+1) <= ty){
		if(tab[place_dans_grille(x+1 , y+1 ,tx,ty)] == 1) nombre_voisin++;
	}

	return nombre_voisin;
}




void appliquer_regles_du_jeu(int* tab,int x, int y ,int mx, int my){
	/* Applique les règles du jeu.
	x,y: coordonnées de la case actuelle à traiter
	mx,my : taille du tableau max)

	FONCTION UTILES POUR CETTE FONCTION : nombre_voisin(tab,x,y,mx,my) == 3 veut dire si la case actuelle a trois voisin
	tab[place_dans_grille(x,y,mx,my)] = 1; veut dire la case actuelle vaut 1
	*/

}

void formatage_commande(char* cmd){
	// Formate la commande pour comprendre au mieux la requête de l'utilisateur
	// (Pour l'instant met tout en minuscule)
	char i;
	while(cmd[i] != '\0'){
		cmd[i] = tolower(cmd[i]);
		i++;
	}
}

void interpreteur_commande(char* cmd, int* tab, int *mx, int *my, int* f){
	//INterprete la commande du l'utilisateur
	/* mx,my : dimensions du tableau
	f : interrupteur pour terminer le programme*/
	char arguments[90],new[100];
	int val,i=0,j=0;


	if(strcmp(cmd,"vie") == 0){
		// Commande vie
		printf("> args:  ");
		lire(arguments,90);
		arguments[strlen(arguments)] = ' ';
		for(i=0; i<strlen(arguments); ++i){
			if(arguments[i] != ' '){
				new[j] = arguments[i];
				j++;
			}
			else{
				j=0;
				val = atoi(new);
				tab[val] = 1;
			}
		}
	}

		else if(strcmp(cmd,"mort") == 0){
			// Commande mort
			printf("> args:  ");
		lire(arguments,90);

		arguments[strlen(arguments)] = ' ';
		for(i=0; i<strlen(arguments); ++i){
			if(arguments[i] != ' '){
				new[j] = arguments[i];
				j++;
			}
			else{
				j=0;
				val = atoi(new);
				tab[val] = 0;
			}
		}
	}


			else if(strcmp(cmd,"stop") == 0){
				// commande stop
				*f = 1;
			}

			else if(strcmp(cmd, "voisin") == 0){
				printf("> "); int p; scanf("%d", &p);
				int xv, yv;
				place_vers_coordonnes(p, &xv, &yv, *mx, *my);
				printf("x:%d y:%d\n", xv, yv);
				printf("V:%d", nombre_voisin(tab, xv, yv, *mx, *my));
				viderBuffer();
			}

			else if(strcmp(cmd,"ok") == 0){
				for(i=0; i<*mx; ++i){
					for(j=0; j<*my; ++ j){
					appliquer_regles_du_jeu(tab,i,j,*mx,*my);
					}
				}
			}
			else printf("commande incorrecte\n");
}